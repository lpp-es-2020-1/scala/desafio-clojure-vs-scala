package Jantar

import akka.actor.{Actor, ActorLogging, ActorRef}
import akka.event.LoggingReceive

import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration.DurationInt

object Filosofos {
  case object comer
  case object pensar
}
class Filosofos(private val garfoEsq: ActorRef,
                private val garfoDir: ActorRef) extends Actor with ActorLogging {
  import garfoMensagem._
  import Filosofos._
  def nome = self.path.name
  implicit private val executionContext: ExecutionContextExecutor = context.dispatcher
  private val comendoTempo = 1000.millis
  private val pensandoTempo = 1500.millis
  private val tentandoTempo = 10.millis
  override def receive: Receive = LoggingReceive {
    case `pensar` =>
      tentandoComer()
  }
  def pensando: Receive = LoggingReceive {
    case `comer` =>
      garfoEsq ! tentouPegar
      garfoDir ! tentouPegar
      context.become(fome)
  }
  def comendo: Receive = LoggingReceive {
    case `pensar` =>
      garfoEsq ! devolver
      garfoDir ! devolver
      context.become(pensando)
      impConsoleComendo()
  }

  def fome: Receive = LoggingReceive {
    case `pegou` if sender equals garfoEsq => {
      context.become(comendoGarfoEsq)
    }
    case `pegou` if sender equals garfoDir => {
      context.become(comendoGarfoDir)
    }
    case `emUso` => {
      context.become(rejectedOnce)
    }
  }
  def comendoGarfoDir: Receive = LoggingReceive {
    case `pegou` if sender equals garfoEsq => {
      context.become(comendo)
      impConsolePensando()
    }
    case `emUso` => {
      garfoDir ! devolver
      tentandoComer()
    }
  }
  def comendoGarfoEsq: Receive = LoggingReceive {
    case `pegou` if sender equals garfoDir =>
      context.become(comendo)
      impConsolePensando()
    case `emUso` =>
      garfoEsq ! devolver
      tentandoComer()
  }
  def rejectedOnce: Receive = LoggingReceive {
    case `pegou` =>
      sender ! devolver
      tentandoComer()
    case `emUso` =>
      tentandoComer()
  }

  private def tentandoComer(): Unit = {
    context.become(pensando)
    retentando()
  }
  private def impConsolePensando(): Unit = {
    //log info "Filosofo: %s esta comendo".format(name)
    println("Filosofo: %s esta comendo".format(nome))
    context.system.scheduler.scheduleOnce(comendoTempo, self, pensar)
  }
  private def impConsoleComendo(): Unit = {
    //log info "Filosofo: %s esta pensando".format(name)
    println("Filosofo: %s esta pensando".format(nome))
    context.system.scheduler.scheduleOnce(pensandoTempo, self, comer)
  }
  private def retentando(): Unit = {
    context.system.scheduler.scheduleOnce(tentandoTempo, self, comer)
  }

}