package Jantar

import akka.actor.{ActorRef, ActorSystem, Props}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt


object Main {
  import Filosofos._
  val system: ActorSystem = ActorSystem("Jantar")
  def main(args: Array[String]): Unit = {
    val tempo=30
    val garfo: Seq[ActorRef] = for (x <- 1 to 5) yield system.actorOf(Props[Garfos], s"c$x")
    val filosofo: Seq[ActorRef] = for ((name, index) <- List("Platao", "Aristoteles", "Socrates", "Rene", "Kant").zipWithIndex)
      yield system.actorOf(Props(classOf[Filosofos], garfo(index), garfo((index + 1) % 5)), name)
    filosofo.foreach(_ ! pensar)
    system.scheduler.scheduleOnce(tempo.seconds)(system.terminate())
  }
}
