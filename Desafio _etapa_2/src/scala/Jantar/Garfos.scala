package Jantar

import akka.actor.{Actor, ActorRef}
import akka.event.LoggingReceive
object garfoMensagem {
  case object tentouPegar
  case object devolver
  case object emUso
  case object pegou
}
class Garfos extends Actor {
  import garfoMensagem._
  override def receive() = idle
  def idle: Receive = LoggingReceive {
    case `tentouPegar` =>
      sender ! pegou
      context.become(busy(sender))
  }
  def busy(philosopherInCharge: ActorRef): Receive = LoggingReceive {
    case `devolver` if sender equals philosopherInCharge =>
      context.become(idle)
    case `tentouPegar` =>
      sender ! emUso
  }
}